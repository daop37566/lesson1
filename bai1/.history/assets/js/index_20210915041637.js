$(document).ready(function(){
  hello_kid_demo();
  hello_teacher_demo();
  var x = 0;
  var audio1 = new Audio("./assets/audio/m1/1 Hello kid - Chào con.mp3");
  var audio2 = new Audio("./assets/audio/m1/2 hello teacher - chào cô giáo.mp3");
  $("#kid-mute-sound").click(function(){
    // $("#kid-mute-sound").css("display","none");
    $("#kid-sound").fadeIn();
    if(x==0){
      audio1.play(); 
    }
    setTimeout(function(){
      audio2.play();
    },1000)
  
    
    x = 1;
  });
  $("#kid-sound").click(function(){
    $("#kid-sound").css("display","none");
    $("#kid-mute-sound").fadeIn();
    audio1.pause();
    audio2.pause();
  });
});


// demo
let playKid ;
let playTeacher ;
function hello_kid_demo(){
  $("#kid-play-kid").click(function(){
    console.log("adas");
    clearTeacher()
    var audio = new Audio("./assets/audio/m1/1 Hello kid - Chào con.mp3");
    playKid =  setInterval(function (){
      audio.play();
    },200)
    $(".kid-sub-void-01").fadeIn();
    $("#kid-sub-void-02").css("display","none");
    $("#kid-play-kid").css("display","none");
    $("#kid-pause-kid").css("display","block");
    $(".kid-01").addClass("active");
  })
  $("#kid-pause-kid").click(function(){
    clearKid()
  })
}
function hello_teacher_demo(){
  $("#kid-play-teacher").click(function(){
    clearKid()
    var audio = new Audio("./assets/audio/m1/2 hello teacher - chào cô giáo.mp3");
    playTeacher =  setInterval(function (){
      audio.play();
    },100)
    $(".kid-sub-void-02").fadeIn();
    $("#kid-sub-void-01").css("display","none");
    $("#kid-play-teacher").css("display","none");
    $("#kid-pause-teacher").css("display","block");
    $(".kid-02").addClass("active");
  });
  $("#kid-pause-teacher").click(function(){
    clearTeacher()
  })
}

function clearTeacher(){
  clearInterval(playTeacher);
    $("#kid-play-teacher").css("display","block");
    $("#kid-pause-teacher").css("display","none");
    $(".kid-02").removeClass("active");
}
function clearKid() {
  clearInterval(playKid);
    $("#kid-play-kid").css("display","block");
    $("#kid-pause-kid").css("display","none");
    $(".kid-01").removeClass("active");
}
